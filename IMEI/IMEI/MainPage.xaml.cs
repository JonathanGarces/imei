﻿using Android.Content;
using Android.Content.PM;
using Android.Telephony;
using Plugin.Permissions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using ZXing.Net.Mobile.Forms;
using static Android.Provider.Settings;

namespace IMEI
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        ZXingBarcodeImageView barcode;
        public MainPage()
        {
            InitializeComponent();
            button.Clicked += Button_Clicked;

        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            //hrow new NotImplementedException();
            var android_id = Android.Provider.Settings.Secure.GetString(Android.App.Application.Context.ContentResolver, Android.Provider.Settings.Secure.AndroidId);

            
            String imei = DependencyService.Get<IServiceImei>().GetImei();

            

            //var telephonyManager = (TelephonyManager)Forms.Context.GetSystemService(Android.Content.Context.TelephonyService);
            //var id = telephonyManager.DeviceId;
            String IMEI = Android.Provider.Settings.Secure.GetString(Forms.Context.ContentResolver, Android.Provider.Settings.Secure.AndroidId);
           // await DisplayAlert("Great!", $"You've gotten the QR code", "OK");
            button.IsVisible = false;
            desc.Text = "Scan QR code to get the EMAI";
            title.Text = "Perfect!";

            barcode = new ZXingBarcodeImageView
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                AutomationId = "zxingBarcodeImageView",
            };
            barcode.BarcodeFormat = ZXing.BarcodeFormat.QR_CODE;
            barcode.BarcodeOptions.Width = 500;
            barcode.BarcodeOptions.Height = 500;
            barcode.BarcodeOptions.Margin = 10;
            barcode.BarcodeValue = IMEI;
            stackPrinc.Children.Add(barcode);
        }

        private object getContext()
        {
            throw new NotImplementedException();
        }
    }
}

public interface IDevice
{
    string GetIdentifier();
}

