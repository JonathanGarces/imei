﻿using System;
using Xamarin.Forms;

using Android.Telephony;
using DemoImei.Droid;


[assembly: Dependency(typeof(ServiceImei))]
namespace DemoImei.Droid
{
    public class ServiceImei : IServiceImei
    {
        [Obsolete]
        public string GetImei()
        {
            try
            {
                TelephonyManager manager = (TelephonyManager)Forms.Context.GetSystemService(Android.Content.Context.TelephonyService);

                return Android.Provider.Settings.Secure.GetString(Forms.Context.ContentResolver, Android.Provider.Settings.Secure.AndroidId);
                //return manager.Imei;
            }
            catch
            {
                return "it is not found";
            }
        }
    }
}

    public interface IServiceImei
    {
        string GetImei();
    }